package facci.davidyturralde.apptarea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText fahrenheitTexto;
    EditText celsiusTexto;
    Button fahrenheitBoton;
    Button celsiusBoton;


    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fahrenheitBoton = (Button) findViewById(R.id.fahrenheitBoton);
        celsiusBoton = (Button) findViewById(R.id.celsiusBoton);
        fahrenheitTexto = (EditText) findViewById(R.id.fahrenheitTexto);
        celsiusTexto = (EditText) findViewById(R.id.celsiusTexto);

        fahrenheitBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double res;
                String res2;
                Double cantidad = Double.parseDouble(fahrenheitTexto.getText().toString());
                res = (cantidad -32)/ 1.8;
                res2 = String.valueOf(res);
                celsiusTexto.setText(res2);
            }
        });


        celsiusBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double res;
                String res2;
                Double cantidad = Double.parseDouble(celsiusTexto.getText().toString());
                res = (cantidad *1.8)+32;
                fahrenheitTexto.setText(""+res);
            }
        });

        Log.d(TAG, "David Yturralde Maldonado");
    }
}
